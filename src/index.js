import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import reactRefresh from './ReactRefresh';

function Header() {
  return (
    <header>
      <h2>Hello World</h2>
    </header>
  );
}

function List(props) {
  const {items} = props;
  const children = items && items.length > 0
    ? items.map((item, index) => <li key={`item-${index}`}>{item}</li>)
    : <li>empty</li>;

  return (
    <ul>
      {children}
    </ul>
  )
}

const RefreshList = reactRefresh(List);

class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: []
    };
  }

  componentDidMount() {
    this.setState({
      list: ['Item 1', 'Item 2']
    });
  }

  render() {
    const {list} = this.state;

    return (
      <section className='content'>
        <RefreshList items={list}/>
      </section>
    );
  }
}

function Footer() {
  return (
    <footer>
      <button type='button'>CLICK ME</button>
    </footer>
  )
}

class App extends Component {
  render() {
    return (
      <div className='container'>
        <Header />
        <Content />
        <Footer />
      </div>
    );
  }
}

ReactDOM.render(<App/>, document.getElementById('app'));
