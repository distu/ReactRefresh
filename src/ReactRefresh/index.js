import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Hammer from 'hammerjs';
import './style.css';

function calculateHeight(resistance, maxHeight, delta) {
  return (-delta * delta + 2 * resistance * maxHeight * delta) / (maxHeight * resistance * resistance);
}

export default function reactRefresh(Wrapped) {
  return class RefreshElement extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loadingHeight: 0,
      };

      this.__event = '';
      this.__startPoint = null;
    }

    componentDidMount() {
      const hammerEle = ReactDOM.findDOMNode(this.refs.hammer);
      const mc = new Hammer(hammerEle);
      mc.get('pan').set({
        direction: Hammer.DIRECTION_VERTICAL,
      });
      mc.on('panstart', (ev) => {
        this.__event = '';
        this.__startPoint = null;
      });
      mc.on('pandown panup', (ev) => {
        let {deltaY, type} = ev;
        console.log(ev.type, ev);
        const resistance = 8;
        const maxHeight = 60;
        deltaY = deltaY >= resistance * maxHeight ? resistance * maxHeight : deltaY;
        const newHeight = calculateHeight(resistance, maxHeight, deltaY);
        this.setState({
          loadingHeight: newHeight,
        })
      });
      mc.on('panend', (ev) => {

      });
    }

    render() {
      const {...others} = this.props;
      const {loadingHeight} = this.state;

      return (
        <div className='refresh-ctn' ref='hammer'>
          <div className='refresh-ctn__loading' style={{ height: loadingHeight }}>
            <p>下拉</p>
          </div>
          <div className='refresh-ctn--inner'>
            <Wrapped {...others}/>
          </div>
        </div>
      );
    }
  }
}
