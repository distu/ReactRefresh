const webpack = require('webpack');

module.exports = {
    context: __dirname + '/src',
    entry: './index.js',
    output: {
        path: './dist',
        filename: './bundle.js',
        publicPath: '/dist/'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['es2015', 'es2016', 'react'],
                plugins: ['transform-object-rest-spread']
            }
        }, {
                test: /\.css$/,
                exclude: /node_modules/,
                loader: 'style!css'
            }]
    }
};